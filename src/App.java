import com.devcamp.task54B_10.Author;
import com.devcamp.task54B_10.Book;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Thuan", "thuan@gmail.com", 'm');
        Author author2 = new Author("Thuan Nguyen", "thuannh@devcamp.vn", 'f');
        System.out.println("Author 1: ");
        System.out.println(author1);
        System.out.println("Author 2: ");
        System.out.println(author2);

        Book book1 = new Book("Java coding in 24h", author1, 200.103);
        Book book2 = new Book("NodeJS coding in 24h", author2, 300.999, 3);
        System.out.println("Book1: ");
        System.out.println(book1);
        System.out.println("Book2: ");
        System.out.println(book2);
    }
}
